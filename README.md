# NEOVIM CONFIGURATION

Neovim configuration using lua programming language.

## Technology

- [Neovim 0.10.0](https://neovim.io/)

## Usage

Clone this repository in the neovim configuration directory.

## License

[GPL-3.0-only](./LICENSE)

## Authors

[AUTHORS](./AUTHORS)
