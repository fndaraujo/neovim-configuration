return ({
    s({ trig = "ctor", dscr = "Create constructor class" },
        fmta(
            [[
                public <>()
                {
                }
            ]],
            { i(1, "ClassName") },
            { delimiters = "<>" }
        )
    ),
    s({ trig = "class", dscr = "Create a class" },
        fmta(
            [[
                <> class <>
                {
                }
            ]],
            {
                i(1, "accessor"),
                i(2, "ClassName")
            },
            { delimiters = "<>" }
        )
    )
})
