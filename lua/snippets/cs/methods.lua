return ({
    s({ trig = "prop", dscr = "Create a property." },
        fmta(
            [[
                public <> <> { get; set; }
            ]],
            {
                i(1, "type"),
                i(2, "Property")
            },
            { delimiters = "<>" }
        )
    ),
    s({ trig = "fn", dscr = "Create a method." },
        fmta(
            [[
                <> <> <>(<>)
                {
                    <>
                }
            ]],
            {
                i(1, "accessor"),
                i(2, "type"),
                i(3, "Name"),
                i(4, "Parameter"),
                i(0)
            },
            { delimiters = "<>" }
        )
    ),
})
