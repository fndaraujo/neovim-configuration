local options = {
    number = true,
    relativenumber = true,
    completeopt = { "menu", "menuone", "noselect" },
    list = true,
    listchars = {
        tab = "| ",
        trail = "·",
        nbsp = "␣"
    }
}

for option, value in pairs(options) do
    vim.opt[option] = value
end
