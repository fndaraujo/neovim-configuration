vim.keymap.set(
    "n",
    "<LEADER>;",
    "A;<ESC>",
    { desc = "Add a semicolon to the end of the line." }
)

vim.keymap.set(
    "n",
    "<LEADER>,",
    "A,<ESC>",
    { desc = "Add a comma to the end of the line." }
)
