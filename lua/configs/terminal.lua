vim.api.nvim_create_autocmd("TermOpen", {
    desc = "Removes numbering in terminal buffer",
    group = vim.api.nvim_create_augroup("custom-term-open", { clear = true }),
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})

vim.keymap.set(
    "n",
    "<LEADER>t",
    function()
        vim.cmd.vnew()
        vim.cmd.term()
        vim.cmd.wincmd("J")
        vim.api.nvim_win_set_height(0, 5)
    end,
    { desc = "Add a terminal buffer at the botton of the current window" }
)
