local globals = {
    mapleader = ";"
}

for global, value in pairs(globals) do
    vim.g[global] = value
end
