local configuration = function()
    local luasnip = require("luasnip")
    luasnip.config.set_config({
        enable_autosnippets = true
    })

    local sniploaders = require("luasnip.loaders.from_lua")
    local snippets = vim.fn.stdpath("config") .. "/lua/snippets"
    sniploaders.lazy_load({paths = snippets})
end

return ({
    "L3MON4D3/LuaSnip",
    version = "v2.*",
    config = configuration
})
