local linters_by_filetype = {
    javascript = { "eslint_d" },
    typescript = { "eslint_d" },
    javascriptreact = { "eslint_d" },
    typescriptreact = { "eslint_d" }
}

local configuration = function()
    local nvimlint = require("lint")
    nvimlint.linters_by_ft = linters_by_filetype
    local nvimlint_augroup = vim.api.nvim_create_augroup(
        "nvimlint",
        { clear = true }
    )
    vim.api.nvim_create_autocmd(
        { "BufEnter", "BufWritePost", "InsertLeave" },
        {
            group = nvimlint_augroup,
            callback = function()
                nvimlint.try_lint()
            end
        }
    )
    vim.keymap.set(
        "n",
        "<LEADER>c",
        function()
            nvimlint.try_lint()
        end,
        { desc = "Trigger linting for current file." }
    )
end

return ({
    "mfussenegger/nvim-lint",
    event = { "BufReadPre", "BufNewFile" },
    config = configuration
})
