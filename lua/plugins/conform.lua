local formatters_by_filetype = {
    c = { "clang-format", cmd = "clang-format -style=file" },
    cpp = { "clang-format", cmd = "clang-format -style=file" },
    javascript = { "prettier" },
    typescript = { "prettier" },
    javascriptreact = { "prettier" },
    typescriptreact = { "prettier" }
}

local configuration = function()
    local conform = require("conform")
    conform.setup({
        formatters_by_ft = formatters_by_filetype,
        format_on_save = {
            async = false,
            timeout_ms = 500,
            lsp_format = "fallback"
        },
        vim.keymap.set(
            { "n", "v" },
            "<LEADER>l",
            function()
                conform.format({
                    async = false,
                    timeout_ms = 500,
                    lsp_format = "fallback"
                })
            end,
            { desc = "Format file or, in visual mode, a range." }
        )
    })
end

return ({
    {
        "stevearc/conform.nvim",
        event = { "BufReadPre", "BufNewFile" },
        config = configuration
    }
})
