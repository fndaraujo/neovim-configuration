local language_parsers = {
    "bash",
    "beancount",
    "bibtex",
    "c",
    "c_sharp",
    "cpp",
    "css",
    "csv",
    "dockerfile",
    "editorconfig",
    "git_config",
    "git_rebase",
    "gitattributes",
    "gitcommit",
    "gitignore",
    "html",
    "http",
    "java",
    "javascript",
    "json",
    "julia",
    "latex",
    "ledger",
    "lua",
    "make",
    "python",
    "scss",
    "sql",
    "styled",
    "toml",
    "tsv",
    "tsx",
    "typescript",
    "vim",
    "vimdoc",
    "vue",
    "xml",
    "yaml"
}

local configuration = function()
    local treesitter_configs = require("nvim-treesitter.configs")

    treesitter_configs.setup({
        ensure_installed = language_parsers,
        sync_install = false,
        highlight = {
            enable = true
        },
        indent = {
            enable = true
        }
    })
end

return ({
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = configuration
    }
})
