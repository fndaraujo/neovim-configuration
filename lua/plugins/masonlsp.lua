local mason_configuration = function()
    local mason = require("mason")
    mason.setup({
        registries = {
            'github:mason-org/mason-registry',
            'github:crashdummyy/mason-registry',
        },
    })
end

local mason_lsp_configuration = function()
    local masonlsp = require("mason-lspconfig")
    masonlsp.setup({
        ensure_installed = {
            "cssls",
            "html",
            "jsonls",
            "ts_ls"
        },
        automatic_installation = {
            exclude = {
                "clangd"
            }
        }
    })
end

local lsp_configuration = function()
    local lspconfig = require("lspconfig")
    local servers = {
        "clangd",
        "cssls",
        "html",
        "jsonls",
        "ts_ls"
    }

    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities.textDocument.completion.completionItem.snippetSupport = true

    if pcall(require, "cmp_nvim_lsp") then
        capabilities = require("cmp_nvim_lsp").default_capabilities()
    else
        local msg = "cmp_nvim_lsp not found! Using default capabilities."
        vim.notify(msg, vim.log.levels.WARN)
    end

    for _, server in pairs(servers) do
        lspconfig[server].setup({
            capabilities = capabilities
        })
    end

    vim.api.nvim_create_autocmd("LspAttach", {
        desc = "LSP actions",
        callback = function()
            local bufmap = function(mode, lhs, rhs)
                local opts = {buffer = true}
                vim.keymap.set(mode, lhs, rhs, opts)
            end
            bufmap("n", "<F2>", "<cmd>lua vim.lsp.buf.rename()<cr>")
        end
    })
end

return ({
    {
        "williamboman/mason.nvim",
        config = mason_configuration
    },
    {
        "williamboman/mason-lspconfig.nvim",
        config = mason_lsp_configuration
    },
    {
        "neovim/nvim-lspconfig",
        config = lsp_configuration
    },
    {
        -- Roslyn language server.
        -- Manual installation requires the download and extraction of the nuget
        -- package for CodeAnalysis. See this url:
        -- https://dev.azure.com/azure-public/vside/_artifacts/feed/vs-impl/NuGet/Microsoft.CodeAnalysis.LanguageServer.linux-x64/overview/4.14.0-1.25061.1
        'seblj/roslyn.nvim',
        ft = { 'cs', 'razor' },
        dependencies = {
            {
                -- By loading as a dependencies, we ensure that we are available to set
                -- the handlers for roslyn
                'tris203/rzls.nvim',
                config = function()
                    ---@diagnostic disable-next-line: missing-fields
                    require('rzls').setup {}
                end,
            },
        },
        config = function()
            require('roslyn').setup {
                args = {
                    '--logLevel=Information',
                    '--extensionLogDirectory=' .. vim.fs.dirname(vim.lsp.get_log_path()),
                    '--razorSourceGenerator=' .. vim.fs.joinpath(
                        vim.fn.stdpath 'data' --[[@as string]],
                        'mason',
                        'packages',
                        'roslyn',
                        'libexec',
                        'Microsoft.CodeAnalysis.Razor.Compiler.dll'
                    ),
                    '--razorDesignTimePath=' .. vim.fs.joinpath(
                        vim.fn.stdpath 'data' --[[@as string]],
                        'mason',
                        'packages',
                        'rzls',
                        'libexec',
                        'Targets',
                        'Microsoft.NET.Sdk.Razor.DesignTime.targets'
                    ),
                },
                ---@diagnostic disable-next-line: missing-fields
                config = {
                    handlers = require 'rzls.roslyn_handlers',
                    settings = {
                        ['csharp|inlay_hints'] = {
                            csharp_enable_inlay_hints_for_implicit_object_creation = true,
                            csharp_enable_inlay_hints_for_implicit_variable_types = true,

                            csharp_enable_inlay_hints_for_lambda_parameter_types = true,
                            csharp_enable_inlay_hints_for_types = true,
                            dotnet_enable_inlay_hints_for_indexer_parameters = true,
                            dotnet_enable_inlay_hints_for_literal_parameters = true,
                            dotnet_enable_inlay_hints_for_object_creation_parameters = true,
                            dotnet_enable_inlay_hints_for_other_parameters = true,
                            dotnet_enable_inlay_hints_for_parameters = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_differ_only_by_suffix = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_match_argument_name = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_match_method_intent = true,
                        },
                        ['csharp|code_lens'] = {
                            dotnet_enable_references_code_lens = true,
                        },
                    },
                },
            }
        end,
        init = function()
            -- we add the razor filetypes before the plugin loads
            vim.filetype.add {
                extension = {
                    razor = 'razor',
                    cshtml = 'razor',
                },
            }
        end,
    }
})
