require("configs.globals")
require("configs.options")

require("configs.lazy")

require("configs.mappings")
require("configs.abbreviations")

require("configs.terminal")
